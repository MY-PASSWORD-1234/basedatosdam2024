-- Álbumes junto al número de canciones en cada uno.
SELECT  A.Title,
    (
        SELECT COUNT(AlbumId)
        FROM Track AS T
        WHERE T.AlbumId = A.AlbumId
    ) AS "NCanciones"
FROM Album AS A;
