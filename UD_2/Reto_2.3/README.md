# Reto 2.3: Consultas con subconsultas II

Link https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_2/Reto_2.3?ref_type=heads
## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.
```sql
use Chinook;
SELECT
 Name
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds)
                FROM Track)
```
### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
```sql
SELECT * FROM Chinook.Invoice i
inner join Chinook.Customer c on c.CustomerId = i.CustomerId
where c.Email = "emma_jones@hotmail.com"
order by i.InvoiceDate desc
limit 5;
```

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.
```sql
SELECT  DISTINCT p.PlaylistId , p.Name FROM Chinook.Playlist p
INNER JOIN Chinook.PlaylistTrack pt ON pt.PlaylistId = p.PlaylistId
WHERE pt.TrackId IN (
    SELECT  TrackId
    FROM Chinook.Track t
    INNER JOIN Chinook.Genre g ON g.GenreId = t.GenreId
    WHERE g.Name = 'Reggae'
);


```
### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.
```sql
SELECT * FROM Chinook.Customer c
INNER JOIN Chinook.Invoice i ON i.CustomerId = c.CustomerId
WHERE i.Total > (
    SELECT 20
);
```
### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.
```sql
use Chinook;
SELECT Album.Title AS Album, 
       Artist.Name AS Artist
FROM Album
JOIN Artist ON Album.ArtistId = Artist.ArtistId
WHERE Album.AlbumId IN (
    SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(TrackId) > 15
);

```

### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.
```sql
SELECT Album.Title AS Album
FROM Album
JOIN (
    SELECT AlbumId, COUNT(*) AS NumTracks
    FROM Track
    GROUP BY AlbumId
) AS TrackCounts ON Album.AlbumId = TrackCounts.AlbumId
WHERE TrackCounts.NumTracks > (
    SELECT AVG(NumTracks)
    FROM (
        SELECT COUNT(*) AS NumTracks
        FROM Track
        GROUP BY AlbumId
    ) AS AvgTrackCounts
);

```
### Consulta 7
Obtener los álbumes con una duración total superior a la media.
```sql
SELECT Album.Title AS Album
FROM Album
JOIN (
    SELECT AlbumId, SUM(Milliseconds) AS TotalDuration
    FROM Track
    GROUP BY AlbumId
) AS AlbumDurations ON Album.AlbumId = AlbumDurations.AlbumId
WHERE AlbumDurations.TotalDuration > (
    SELECT AVG(TotalDuration)
    FROM (
        SELECT SUM(Milliseconds) AS TotalDuration
        FROM Track
        GROUP BY AlbumId
    ) AS AvgAlbumDurations
);
 
```
### Consulta 8
Canciones del género con más canciones.
```sql
select *
from Track t
inner join Genre g on t.GenreId = g.GenreId
where t.GenreId = (
	 select GenreId
     from  (
		SELECT GenreId , COUNT(*) AS NumeroCanciones
		FROM Track t
		GROUP BY GenreId 
		order by NumeroCanciones desc
		limit 1
		)as  canciones
);

```
### Consulta 9
Canciones de la _playlist_ con más canciones.
```sql
SELECT Track.Name
FROM Track
INNER JOIN PlaylistTrack pt on Track.TrackId = pt.TrackId
WHERE pt.PlaylistId = (SELECT PlaylistId
                       FROM PlaylistTrack
                       GROUP BY PlaylistId
                       ORDER BY COUNT(TrackId) DESC
                       LIMIT 1);

```

## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
```sql
SELECT CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT SUM(Total)
        FROM Invoice AS I
        WHERE I.CustomerId = C.CustomerId
    ) AS Total
FROM Customer AS C;

```
### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
```sql
SELECT  CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT COUNT(CustomerId)
        FROM Customer AS C
        WHERE C.SupportRepId = E.EmployeeId
    ) AS 'Sirven'
FROM Employee AS E;

```
### Consulta 12
Ventas totales de cada empleado.
```sql
SELECT CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT 
            SUM(
                (SELECT SUM(Total)
                FROM Invoice AS I
                WHERE I.CustomerId = C.CustomerId)
            ) AS 'Total'
        FROM Customer AS C
        WHERE SupportRepId = E.EmployeeId
        GROUP BY SupportRepId
    ) as VentasTotales
FROM Employee E

```
### Consulta 13
Álbumes junto al número de canciones en cada uno.
```sql
SELECT  A.Title,
    (
        SELECT COUNT(AlbumId)
        FROM Track AS T
        WHERE T.AlbumId = A.AlbumId
    ) AS "NCanciones"
FROM Album AS A;

```
### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
```sql
SELECT  Name, Vieja
    FROM (
        SELECT Ar.ArtistId, Ar.Name,
            (
                SELECT Title
                FROM Album AS Al
                WHERE Al.ArtistId = Ar.ArtistId
                ORDER BY Al.AlbumId
                LIMIT 1
            ) AS Vieja
        FROM Artist AS Ar
    ) AS sub
WHERE Vieja IS NOT NULL;


```