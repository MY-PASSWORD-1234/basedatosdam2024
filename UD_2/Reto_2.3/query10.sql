-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
SELECT CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT SUM(Total)
        FROM Invoice AS I
        WHERE I.CustomerId = C.CustomerId
    ) AS Total
FROM Customer AS C;
