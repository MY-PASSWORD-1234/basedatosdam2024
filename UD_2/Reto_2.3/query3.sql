SELECT  DISTINCT p.PlaylistId , p.Name FROM Chinook.Playlist p
INNER JOIN Chinook.PlaylistTrack pt ON pt.PlaylistId = p.PlaylistId
WHERE pt.TrackId IN (
    SELECT  TrackId
    FROM Chinook.Track t
    INNER JOIN Chinook.Genre g ON g.GenreId = t.GenreId
    WHERE g.Name = 'Reggae'
);