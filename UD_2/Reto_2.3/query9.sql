-- Canciones de la _playlist_ con más canciones.
use Chinook;
SELECT Track.Name
FROM Track
INNER JOIN PlaylistTrack pt on Track.TrackId = pt.TrackId
WHERE pt.PlaylistId = (SELECT PlaylistId
                       FROM PlaylistTrack
                       GROUP BY PlaylistId
                       ORDER BY COUNT(TrackId) DESC
                       LIMIT 1);
