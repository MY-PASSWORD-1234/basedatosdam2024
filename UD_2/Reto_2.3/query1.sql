use Chinook;
SELECT
 Name
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds)
                FROM Track)