-- Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.
SELECT  Name, Vieja
    FROM (
        SELECT Ar.ArtistId, Ar.Name,
            (
                SELECT Title
                FROM Album AS Al
                WHERE Al.ArtistId = Ar.ArtistId
                ORDER BY Al.AlbumId
                LIMIT 1
            ) AS Vieja
        FROM Artist AS Ar
    ) AS sub
WHERE Vieja IS NOT NULL;

