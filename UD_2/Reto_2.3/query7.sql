SELECT Album.Title AS Album
FROM Album
JOIN (
    SELECT AlbumId, SUM(Milliseconds) AS TotalDuration
    FROM Track
    GROUP BY AlbumId
) AS AlbumDurations ON Album.AlbumId = AlbumDurations.AlbumId
WHERE AlbumDurations.TotalDuration > (
    SELECT AVG(TotalDuration)
    FROM (
        SELECT SUM(Milliseconds) AS TotalDuration
        FROM Track
        GROUP BY AlbumId
    ) AS AvgAlbumDurations
);
