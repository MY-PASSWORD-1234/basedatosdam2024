-- Ventas totales de cada empleado.
SELECT CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT 
            SUM(
                (SELECT SUM(Total)
                FROM Invoice AS I
                WHERE I.CustomerId = C.CustomerId)
            ) AS 'Total'
        FROM Customer AS C
        WHERE SupportRepId = E.EmployeeId
        GROUP BY SupportRepId
    ) as VentasTotales
FROM Employee E
