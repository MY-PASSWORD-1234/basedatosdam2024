-- Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
SELECT  CONCAT(FirstName, ' ', LastName) AS Nombre_Completo,
    (
        SELECT COUNT(CustomerId)
        FROM Customer AS C
        WHERE C.SupportRepId = E.EmployeeId
    ) AS 'Sirven'
FROM Employee AS E;
