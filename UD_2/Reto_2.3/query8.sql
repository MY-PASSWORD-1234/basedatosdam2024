select *
from Track t
inner join Genre g on t.GenreId = g.GenreId
where t.GenreId = (
	 select GenreId
     from  (
		SELECT GenreId , COUNT(*) AS NumeroCanciones
		FROM Track t
		GROUP BY GenreId 
		order by NumeroCanciones desc
		limit 1
		)as  canciones
);