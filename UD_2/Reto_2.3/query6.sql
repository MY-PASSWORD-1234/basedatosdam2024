-- Obtener los álbumes con un número de canciones superiores a la media.
SELECT Album.Title AS Album
FROM Album
JOIN (
    SELECT AlbumId, COUNT(*) AS NumTracks
    FROM Track
    GROUP BY AlbumId
) AS TrackCounts ON Album.AlbumId = TrackCounts.AlbumId
WHERE TrackCounts.NumTracks > (
    SELECT AVG(NumTracks)
    FROM (
        SELECT COUNT(*) AS NumTracks
        FROM Track
        GROUP BY AlbumId
    ) AS AvgTrackCounts
);

