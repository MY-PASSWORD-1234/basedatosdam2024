use Chinook;
SELECT Album.Title AS Album, 
       Artist.Name AS Artist
FROM Album
JOIN Artist ON Album.ArtistId = Artist.ArtistId
WHERE Album.AlbumId IN (
    SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(TrackId) > 15
);

