SELECT g.Name AS Genre, COUNT(Distinct t.Name) AS NumeroCanciones
FROM Genre g
INNER JOIN Track t ON t.GenreId = g.GenreId
GROUP BY g.Name;