SELECT p.Name as Album, t.Name as Nombre_Cancion , t.Milliseconds as Duracion
FROM Chinook.Playlist p
inner join Chinook.PlaylistTrack pl on p.PlaylistId = pl.PlaylistId
inner join Chinook.Track t on pl.TrackId = t.TrackId
inner join Chinook.Album a on t.AlbumId = a.AlbumId
where p.Name like "C%"
order by t.Milliseconds desc, Album desc;