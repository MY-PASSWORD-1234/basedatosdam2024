USE Chinook;
SELECT a.Title AS Album, COUNT(*) AS Ventas
FROM Track t
INNER JOIN InvoiceLine il ON il.TrackId = t.TrackId
INNER JOIN Album a ON t.AlbumId = a.AlbumId
GROUP BY a.Title
ORDER BY Ventas DESC
LIMIT 6;