SELECT p1.LastName, p1.FirstName, p2.LastName , p2.FirstName
FROM Chinook.Employee as p1 join Chinook.Employee as p2 on p1.EmployeeId = p2.ReportsTo 
order by p1.BirthDate desc
limit 15;