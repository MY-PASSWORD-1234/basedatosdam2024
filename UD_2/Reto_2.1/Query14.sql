use Chinook;
SELECT distinct count(t.Name) 
FROM Chinook.Album a
inner join Track t on t.AlbumId = a.AlbumID
where a.Title = "Out Of Time";