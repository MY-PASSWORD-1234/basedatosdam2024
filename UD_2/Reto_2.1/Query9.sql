SELECT i.InvoiceDate as Fecha_Factura, concat(c.FirstName ," ",c.LastName )as Nombre_Completo, c.Address as Direccion, c.PostalCode as Codigo_Postal, c.Country as Pais, i.Total  as Importe
FROM Chinook.Invoice i
join Chinook.Customer c on i.CustomerId = c.CustomerId
where i.BillingCity like "Berlin";