use Chinook;
SELECT g.Name AS Genre, COUNT(*) AS Ventas
FROM Genre g
INNER JOIN Track t ON t.GenreId = g.GenreId
INNER JOIN InvoiceLine il ON il.TrackId = t.TrackId
GROUP BY g.Name
ORDER BY Ventas DESC
limit 10;