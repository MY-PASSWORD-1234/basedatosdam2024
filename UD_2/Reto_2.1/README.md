
Alex Asensio 
------
# Reto 2.1

Mi link de mi repositorio es este  https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_2/Reto_2.1?ref_type=heads

## Query 1
Encuentra todos los clientes de Francia.

```sql
SELECT FirstName,LastName
FROM Chinook.Customer
where Country like 'France';

```


## Query 2
Muestra las facturas del primer trimestre de este año.


```sql
SELECT * 
FROM Chinook.Invoice 
where InvoiceDate >= '2024-01-01' and InvoiceDate <='2024-03-31' ;

```


## Query 3
Muestra todas las canciones compuestas por AC/DC.
```sql
SELECT Name as Cancion , Composer as Compositor 
FROM Chinook.Track 
where Composer like 'AC/DC';

```
## Query 4
Muestra las 10 canciones que más tamaño ocupan.
```sql
SELECT Name as cancion, Bytes FROM Chinook.Track
order by Bytes desc 
limit 10

```
## Query 5
Muestra el nombre de aquellos países en los que tenemos clientes.
```sql
SELECT distinct Country
FROM Chinook.Customer
where FirstName is not null;

```
## Query 6
Muestra todos los géneros musicales.
```sql
SELECT name 
FROM Chinook.Genre;

```
# Consultas sobre múltiples tablas

## Query 7
Muestra todos los artistas junto a sus álbumes.
```sql
SELECT a.Name as Artista , al.title  as Nombre_Album
FROM Chinook.Artist a
inner join Chinook.Album al on a.ArtistId = al.ArtistId ;

```
## Query 8
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de
sus supervisores, si los tienen.
```sql
SELECT p1.LastName, p1.FirstName, p2.LastName , p2.FirstName
FROM Chinook.Employee as p1 join Chinook.Employee as p2 on p1.EmployeeId = p2.ReportsTo 
order by p1.BirthDate desc
limit 15;
```
## Query 9
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las colum-
nas: fecha de la factura, nombre completo del cliente, dirección de facturación,
código postal, país, importe (en este orden)
```sql
SELECT i.InvoiceDate as Fecha_Factura, concat(c.FirstName ," ",c.LastName )as Nombre_Completo, c.Address as Direccion, c.PostalCode as Codigo_Postal, c.Country as Pais, i.Total  as Importe
FROM Chinook.Invoice i
join Chinook.Customer c on i.CustomerId = c.CustomerId
where i.BillingCity like "Berlin";
```
## Query 10
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
sus canciones, ordenadas por álbum y por duración.
```sql
SELECT p.Name as Album, t.Name as Nombre_Cancion , t.Milliseconds as Duracion
FROM Chinook.Playlist p
inner join Chinook.PlaylistTrack pl on p.PlaylistId = pl.PlaylistId
inner join Chinook.Track t on pl.TrackId = t.TrackId
inner join Chinook.Album a on t.AlbumId = a.AlbumId
where p.Name like "C%"
order by t.Milliseconds desc, Album desc;
```
## Query 11
Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
```sql
SELECT c.LastName as Apellido, i.Total 
FROM Chinook.Invoice i
inner join Chinook.Customer c on i.CustomerId = c.CustomerId
where i.Total >="10"
order by c.LastName asc ;
```

# Consultas con funciones de agregación

## Query 12
Muestra el importe medio, mínimo y máximo de cada factura.

```sql
SELECT c.LastName as Apellido, i.Total 
FROM Chinook.Invoice i
inner join Chinook.Customer c on i.CustomerId = c.CustomerId
where i.Total >="10"
order by c.LastName asc ;
```

## Query 13
Muestra el número total de artistas.

```sql
SELECT distinct count(Name) 
FROM Chinook.Artist;
```
## Query 14
Muestra el número de canciones del álbum “Out Of Time”

```sql
use Chinook;
SELECT distinct count(t.Name) 
FROM Chinook.Album a
inner join Track t on t.AlbumId = a.AlbumID
where a.Title = "Out Of Time";
```
## Query 15
Muestra el número de canciones del álbum “Out Of Time”

```sql
SELECT  count(distinct Country) 
FROM Chinook.Customer 
where country is not null ;
```

## Query 16
Muestra el número de canciones de cada género (deberá mostrarse el nombre del
género).

```sql
SELECT g.Name AS Genre, COUNT(Distinct t.Name) AS NumeroCanciones
FROM Genre g
INNER JOIN Track t ON t.GenreId = g.GenreId
GROUP BY g.Name;
```

## Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

```sql
SELECT a.Title, COUNT(DISTINCT t.Name) AS numero
FROM Album a
INNER JOIN Track t ON t.AlbumId = a.AlbumId
GROUP BY a.Title
ORDER BY numero DESC;
```
## Query 18
Encuentra los géneros musicales más populares (los más comprados).

```sql
use Chinook;
SELECT g.Name AS Genre, COUNT(*) AS Ventas
FROM Genre g
INNER JOIN Track t ON t.GenreId = g.GenreId
INNER JOIN InvoiceLine il ON il.TrackId = t.TrackId
GROUP BY g.Name
ORDER BY Ventas DESC
limit 10;
```
## Query 19
Lista los 6 álbumes que acumulan más compras.

```sql
USE Chinook;
SELECT a.Title AS Album, COUNT(*) AS Ventas
FROM Track t
INNER JOIN InvoiceLine il ON il.TrackId = t.TrackId
INNER JOIN Album a ON t.AlbumId = a.AlbumId
GROUP BY a.Title
ORDER BY Ventas DESC
LIMIT 6;
```
## Query 20
Muestra los países en los que tenemos al menos 5 clientes.

```sql
SELECT Country, COUNT(Country)
FROM Customer
GROUP BY Country
HAVING COUNT(Country) >= 5;
```

