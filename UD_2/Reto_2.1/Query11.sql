SELECT c.LastName as Apellido, i.Total 
FROM Chinook.Invoice i
inner join Chinook.Customer c on i.CustomerId = c.CustomerId
where i.Total >="10"
order by c.LastName asc ;