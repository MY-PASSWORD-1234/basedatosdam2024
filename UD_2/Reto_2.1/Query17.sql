SELECT a.Title, COUNT(DISTINCT t.Name) AS numero
FROM Album a
INNER JOIN Track t ON t.AlbumId = a.AlbumId
GROUP BY a.Title
ORDER BY numero DESC;