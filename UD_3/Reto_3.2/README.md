 
Alex Asensio 
------
# Reto 3.2
- Link para llegar al repositorio https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_3/Reto_3.2?ref_type=heads

# Consola Comandos
## Como se accede a una base de datos a trabes de la terminal ?
```
mysql -u "usuario" -p "con esto le dices que luego te pida la contraseña" -P "puerto" -h "host al que te quieres conectar"
```
## Como mostrar todos los usuarios existentes y desde que host te puedes conectar

```sql
select host,user
from mysql.user;
-- en algunos host nos muestra un % que significa que te puedes conectar desde cualquier sitio
```
 
## Como ver los privilegios que tiene cada usuario

```sql
show grants for 'root'@'%';
```
# Como dar de alta un usuarios
## Usuario Admin
```sql
CREATE USER 'admin'@'%' IDENTIFIED BY 'contraseña';
-- con este le damos todos lo privilegios
GRANT ALL PRIVILEGES ON  Chinook. * TO 'admin'@'%';
-- para que se hagan efecto los privilegios
FLUSH PRIVILEGES;
-- si le quieres quitar todos los privilegios algun usuario de alguna database
REVOKE ALL PRIVILEGES ON Chinook.* FROM 'admin'@'%';
```

## Usuario User
```sql
CREATE USER 'user'@'%' IDENTIFIED BY 'contraseña';
-- con este le damos todos lo privilegios
GRANT SELECT ON  Chinook.Playlist TO 'user'@'%';
GRANT SELECT ON  Chinook.PlaylistTrack TO 'user'@'%';
GRANT SELECT ON  Chinook.Track TO 'user'@'%';
GRANT SELECT ON  Chinook.Artist TO 'user'@'%';
-- para que se hagan efecto los privilegios
FLUSH PRIVILEGES;

```

## Usuario Empleado
```sql
CREATE USER 'empleado'@'%' IDENTIFIED BY 'contraseña';
-- con este le damos todos lo privilegios
GRANT ALL PRIVILEGES ON  Chinook. * TO 'empleado'@'%';
REVOKE SELECT ON Chinook.Inovice FROM 'empleado'@'%';
REVOKE SELECT ON Chinook.InoviceLine FROM 'empleado'@'%';
REVOKE SELECT ON Chinook.Employee FROM 'empleado'@'%';
-- para que se hagan efecto los privilegios
FLUSH PRIVILEGES;

```

## Crear Roles
```sql
CREATE ROLE user;
--para darle privilegios 
GRANT SELECT ON  Chinook.Playlist TO user;
GRANT SELECT ON  Chinook.PlaylistTrack TO user;
GRANT SELECT ON  Chinook.Track TO user;
GRANT SELECT ON  Chinook.Artist TO user;
-- para que se hagan efecto los privilegios
FLUSH PRIVILEGES;
-- y para ponerselo a un usuario 
GRANT user to 'user'@'%';
SET ROLE "user";
-- mostrar tus roles
SELECT CURRENT ROLE;
```



## Como cambiar el host o el nombre de un usuario existente
```sql
RENAME USER 'admin'@'%' TO 'usuario_nuevo'@'host_nuevo' ;
```

## Que permisos puede tener un usuario

   - SELECT: Permite al usuario seleccionar datos de una tabla o vista.
  -  INSERT: Permite al usuario insertar nuevos registros en una tabla.
  -  UPDATE: Permite al usuario modificar registros existentes en una tabla.
  -  DELETE: Permite al usuario eliminar registros de una tabla.
  -  CREATE: Permite al usuario crear nuevos objetos en la base de datos, como tablas, vistas, índices, etc.
  -  ALTER: Permite al usuario alterar la estructura de los objetos existentes en la base de datos, como modificar una tabla añadiendo una columna, por ejemplo.
  -  DROP: Permite al usuario eliminar objetos de la base de datos, como tablas, vistas, índices, etc.
  -  GRANT: Permite al usuario otorgar privilegios a otros usuarios.
  -  REVOKE: Permite al usuario retirar privilegios que haya otorgado previamente a otros usuarios.
   - ALL PRIVILEGES: Concede todos los permisos disponibles sobre un objeto o base de datos específica.

# Qué comandos usamos para gestionar los usuarios y sus permisos
## Crear usuario
```sql
CREATE USER 'admin'@'host' IDENTIFIED BY 'contraseña';
```
## Dar permisos
```sql
GRANT ALL PRIVILEGES ON chinook TO 'admin'@'host';
```
## Quitar permisos
```sql
REVOKE ALL PRIVILEGES ON chinook TO 'admin'@'host';
```
## Bloquear usuario
```sql
ALTER USER 'admin'@'%' ACCOUNT LOCK;
-- desbloquear
ALTER USER 'admin'@'%' ACCOUNT UNLOCK;
-- parar saber todas las sesiones abiertas
Select id from information_schema.processlist where user = "admin";
```
## Eliminar usuario
```sql
DROP USER 'admin'@'%';
```
