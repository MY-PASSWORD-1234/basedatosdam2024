

Alex Asensio 
------


#  Creacion De DataBase 
Creamos la database con el comando `Create database`

```sql
CREATE DATABASE reservaVuelos
```


# Creacion De Tablas
A continuacion creo las tres tablas con el comando `Create table` y le especifico los campos y que limites tendran con `NOT NULL` si quiero que ese campo no este nunca vacion, `Auto_increment` si quiero que se ponga el numero de manera automatica y luego les especifico de que tipo sera cada campo como por ejemplo `int` si quiero que sea un numero o `varchar` si quiero que sean palabras y numeros y `Primary key` para el campo que quieras que sea unico en esa tabla. 

```sql
--Aqui creo la tabla Pasajeros con sus campos y su primari key que es id_pasajero
CREATE TABLE `Pasajeros` (
  `id_pasajero` int unsigned NOT NULL AUTO_INCREMENT, --int porque es lo mas util para los id y auto increment para que se ponga automaticamente y primari key porque es el campo por el cual voy a identificar a los pasajeros
  `numero_pasaporte` varchar(50) NOT NULL DEFAULT '', --Aqui he eleigo NOT NULL  porque me interesa que no este vacio el campo de numero de pasaporte y le pongo varchar porque un pasaporte es un codigo estilo al DNI numero y letras 
  `nombre_pasajero` varchar(50) NOT NULL DEFAULT '', --Aqui he eleigo NOT NULL porque me interesa que no este vacio el campo de nombre usuario no este vacio y un varchar porque son caracteres los nombres
  PRIMARY KEY (`id_pasajero`,`numero_pasaporte`) USING BTREE 
)
--Aqui creo la tabla Vuelos con sus campos y su primari key que es id_vuelos
CREATE TABLE `Vuelos` (
  `id_vuelo` int unsigned NOT NULL AUTO_INCREMENT, --int porque es lo mas util para los id y auto increment para que se ponga automaticamente y primari key porque es el campo por el cual voy a identificar a los vuelos
  `origen` varchar(50) NOT NULL DEFAULT '', --Aqui he eleigo NOT NULL porque me interesa que no este vacio el campo de origen no este vacio y un varchar porque son caracteres los nombres
  `destino` varchar(50) NOT NULL DEFAULT '',--Aqui he eleigo NOT NULL porque me interesa que no este vacio el campo de  destino no este vacio y un varchar porque son caracteres los nombres
  `fecha` date NOT NULL DEFAULT (0), --Aqui he eleigo NOT NULL porque me interesa que no este vacio el campo de fecha no este vacio y un date con el siguiente modo de introduccion yyyy-mm-dd
  `capacidad` int NOT NULL DEFAULT (0), --Aqui he eleigo NOT NULL porque me interesa que no este vacio el campo de capacidad y un int porque es un numero lo que quiero poner hay.
  PRIMARY KEY (`id_vuelo`)
)
--Aqui creo la tabla Vuelos_Pasajeros o reservas con su Primari key id_reserva y las claves foranias id_vuelo y id_pasajero
CREATE TABLE `Vuelos_Pasajeros` (
  `id_reserva` int NOT NULL AUTO_INCREMENT,--int porque es lo mas util para los id y auto increment para que se ponga automaticamente y primari key porque es el campo por el cual voy a identificar a los Vuelos_Pasajero o reserva
  `id_vuelo` int unsigned NOT NULL, -- Aqui es una clave foranea y NOT NULL para que no este vacio el campo de id_vuelo
  `id_pasajero` int unsigned NOT NULL,-- Aqui es una clave foranea y NOT NULL para que no este vacio el campo de id_pasajero
  `n_asiento` int DEFAULT NULL, -- esto es un int porque quiero que los numeros de asiento sean solo numeros y no numeros y letras
  PRIMARY KEY (`id_reserva`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `id_pasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),-- aqui defino la clave foranea de pasajeros 
  CONSTRAINT `id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)-- aqui defino la clave foranea de vuelos 
)
```

# Inserto Datos En La Tablas
En esta parte añado con `INSERT INTO VALUES()` todos los datos de las tablas.
```sql
--AÑADO DATOS EN PASAJEROS
INSERT INTO `Pasajeros` VALUES (1,'AB123456','John Smith'),(2,'CD789012','Emma Johnson'),(3,'EF345678','Michael Brown'),(4,'GH901234','Emily Williams'),(5,'IJ567890','William Davis'),(6,'KL123456','Olivia Jones'),(7,'MN789012','James Wilson'),(8,'OP345678','Sophia Taylor'),(9,'QR901234','Alexander Martinez'),(10,'ST567890','Charlotte Garcia'),(11,'UV123456','Daniel Rodriguez'),(12,'WX789012','Isabella Hernandez'),(13,'YZ345678','Matthew Lopez'),(14,'AB901234','Amelia Gonzalez'),(15,'CD567890','Ethan Perez'),(16,'EF123456','Mia Sanchez'),(17,'GH789012','Benjamin Rivera'),(18,'IJ345678','Evelyn Torres'),(19,'KL901234','Andrew Flores'),(20,'MN567890','Avery Ramirez'),(21,'OP123456','Sofia Cruz'),(22,'QR789012','Jackson Moore'),(23,'ST345678','Ava Gomez'),(24,'UV901234','Lucas Reed'),(25,'WX567890','Harper Price'),(26,'YZ123456','Jackson Long'),(27,'AB789012','Scarlett King'),(28,'CD345678','Henry Young'),(29,'EF901234','Madison Bell'),(30,'GH567890','Carter Howard'),(31,'IJ123456','Victoria Cook'),(32,'KL789012','Mateo Baker'),(33,'MN345678','Penelope Turner'),(34,'OP901234','Gabriel Adams'),(35,'QR567890','Grace Morris'),(36,'ST123456','Ryan Hill'),(37,'UV789012','Luna Mitchell'),(38,'WX345678','Leo Perez'),(39,'YZ901234','Liam Thompson'),(40,'AB567890','Zoe Nelson'),(41,'CD123456','Elijah White'),(42,'EF789012','Lily Martin'),(43,'GH345678','Nathan Scott'),(44,'IJ901234','Audrey Green'),(45,'KL567890','Joseph Baker'),(46,'MN123456','Stella Martinez'),(47,'OP789012','David Nguyen'),(48,'QR345678','Aria Carter'),(49,'ST901234','Jack Hill'),(50,'UV567890','Emma Phillips'),(51,'PO987564','Pablo Ruíz');
--AÑADO DATOS EN VUELOS
INSERT INTO `Vuelos` VALUES (1,'New York','Los Angeles','2024-03-21',200),(2,'Los Angeles','Chicago','2024-03-22',180),(3,'Chicago','Houston','2024-03-23',220),(4,'Houston','Miami','2024-03-24',190),(5,'Miami','Denver','2024-03-25',210),(6,'Denver','San Francisco','2024-03-26',180),(7,'San Francisco','Seattle','2024-03-27',220),(8,'Seattle','Las Vegas','2024-03-28',200),(9,'Las Vegas','New York','2024-03-29',190),(10,'New York','Dallas','2024-03-30',220),(11,'Dallas','Atlanta','2024-03-31',210),(12,'Atlanta','Phoenix','2024-04-01',180),(13,'Phoenix','Boston','2024-04-02',200),(14,'Boston','Washington DC','2024-04-03',190),(15,'Washington DC','San Diego','2024-04-04',220),(16,'San Diego','Minneapolis','2024-04-05',210),(17,'Minneapolis','Detroit','2024-04-06',180),(18,'Detroit','Philadelphia','2024-04-07',200),(19,'Philadelphia','San Antonio','2024-04-08',190),(20,'San Antonio','Portland','2024-04-09',220),(21,'Portland','St. Louis','2024-04-10',210),(22,'St. Louis','Charlotte','2024-04-11',180),(23,'Charlotte','Orlando','2024-04-12',200),(24,'Orlando','Salt Lake City','2024-04-13',190),(25,'Salt Lake City','Tampa','2024-04-14',220),(26,'Tampa','Cincinnati','2024-04-15',210),(27,'Cincinnati','Kansas City','2024-04-16',180),(28,'Kansas City','Indianapolis','2024-04-17',200),(29,'Indianapolis','Raleigh','2024-04-18',190),(30,'Raleigh','Nashville','2024-04-19',220),(31,'Nashville','Las Vegas','2024-04-20',210),(32,'Las Vegas','San Francisco','2024-04-21',180),(33,'San Francisco','Chicago','2024-04-22',200),(34,'Chicago','New York','2024-04-23',190),(35,'New York','Los Angeles','2024-04-24',220),(36,'Los Angeles','Miami','2024-04-25',210),(37,'Miami','Dallas','2024-04-26',180),(38,'Dallas','Seattle','2024-04-27',200),(39,'Seattle','Boston','2024-04-28',190),(40,'Boston','Phoenix','2024-04-29',220),(41,'Phoenix','Washington DC','2024-04-30',210),(42,'Washington DC','San Diego','2024-05-01',180),(43,'San Diego','Detroit','2024-05-02',200),(44,'Detroit','Portland','2024-05-03',190),(45,'Portland','Charlotte','2024-05-04',220),(46,'Charlotte','Salt Lake City','2024-05-05',210),(47,'Salt Lake City','Tampa','2024-05-06',180),(48,'Tampa','Cincinnati','2024-05-07',200),(49,'Cincinnati','Kansas City','2024-05-08',190),(50,'Kansas City','Indianapolis','2024-05-09',220),(51,'Valencia','Barcelona','2024-04-01',200);
--AÑADO DATOS EN Vuelos_Pasajero
INSERT INTO `Vuelos_Pasajeros` VALUES (2,2,2,5),(3,3,3,18),(4,4,4,8),(5,5,5,21),(6,6,6,10),(7,7,7,15),(8,8,8,9),(9,9,9,24),(10,10,10,3),(11,11,11,16),(12,12,12,7),(13,13,13,20),(14,14,14,11),(15,15,15,14),(16,16,16,19),(17,17,17,2),(18,18,18,25),(19,19,19,6),(20,20,20,13),(21,21,21,22),(22,22,22,1),(23,23,23,17),(24,24,24,4),(25,25,25,23),(26,26,26,12),(27,27,27,5),(28,28,28,18),(29,29,29,8),(30,30,30,21),(31,31,31,10),(32,32,32,15),(33,33,33,9),(34,34,34,24),(35,35,35,3),(36,36,36,16),(37,37,37,7),(38,38,38,20),(39,39,39,11),(40,40,40,14),(41,41,41,19),(42,42,42,2),(43,43,43,25),(44,44,44,6),(45,45,45,13),(46,46,46,22),(47,47,47,1),(48,48,48,17),(49,49,49,4),(50,50,50,23),(51,1,1,34);
```

## PORQUE ES NECESARIO 3 TABLAS ?
Porque una es para los Pasajeros con toda su informacion otra es para los Vuelos y todo su informacion y otra para reservas donde se encuntra informacion de Pasajeros y Vuelos a traves de una Clave Foranea para cada una

## Como explorar las bases de datos 
Se puede utilizar el comando `Show Databases` que este comando te devolvera un listado de todas las bases de datos disponibles en tu MySQL.

## Como explorar las tablas de una BD
Se puede utilizar el comando `Show Tables` que este comado te devolvera una lista de todas las tablas que tiene esa BD.

## Que uso tienen Commit , Rollback y Autocommit

### COMMIT
`Commit` se utiliza para confirmar los cambios que se han hecho en una consulta y hacer que sean permanentes en la base de datos , despues de utilizar el comando `Commit` los cambios realizados dentro de esa consulta se guardaran en la base de datos y no se podran revertir.

### ROLLBACK
`Rollback` se utiliza para revertir los cambios que se han hecho en una consulta o deshacerlos hasta el ultimo `Commit`.

### AUTOCOMMIT
`Autocommit` se utiliza para que en vez de que tengas que hacer un `Commit` por cada consulta se haga automaticamente despues de cada una de ellas, y si esta deshabilitado el `Autocommit` deberas hacer un `Commit` si quieres confirmar los cambios.

Para habilitarlo es con el siguiente comando:
```sql
SET AUTOCOMMIT = 1;
```

Para deshabilitarlo es con el siguiente comando:
```sql
SET AUTOCOMMIT = 0;
```

