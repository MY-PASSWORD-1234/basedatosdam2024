
Alex Asensio 
------
# Reto 1

Mi link de mi repositorio es este  https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_1/Ejercicio_sanidad

## Query 1
Muestre los hospitales existentes (número, nombre y teléfono).

```sql
use sanitat;
select NOM ,HOSPITAL_COD ,TELEFON
from HOSPITAL;

```


## Query 2
Muestre los hospitales existentes (número, nombre y teléfono) que tengan
una letra A en la segunda posición del nombre.


```sql
use sanitat;
select NOM ,HOSPITAL_COD ,TELEFON
from HOSPITAL
Where NOM Like '_a%';

```


## Query 3
Muestre los trabajadores (código hospital, código sala, número empleado y
apellido) existentes.
```sql
use sanitat;
select HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM
from PLANTILLA;

```
## Query 4
Muestre los trabajadores (código hospital, código sala, número empleado y
apellido) que no sean del turno de noche.
```sql
use sanitat;
select HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM,TORN
from PLANTILLA
where TORN Not like 'N' ;

```
## Query 5
Muestre a los enfermos nacidos en 1960.
```sql
use sanitat;
select *
from MALALT
where  year(DATA_NAIX) = '1960';

```
## Query 6
Muestre a los enfermos nacidos a partir del año 1960.
```sql
use sanitat;
select *
from MALALT
where  year(DATA_NAIX) >= '1960';

```



