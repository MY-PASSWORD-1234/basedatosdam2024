
Alex Asensio 
------
# Reto 1_2

Mi link de mi repositorio es este  https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_1/Reto_1.2?ref_type=heads


# Empresa
## Query 1
Muestre los productos (código y descripción) que comercializa la empresa.

```sql
use empresa;
select p.DESCRIPCIO,p.PROD_NUM
from PRODUCTE p;

```


## Query 2
Muestre los productos (código y descripción) que contienen la palabra tenis
en la descripción.


```sql
use empresa;
select p.DESCRIPCIO,p.PROD_NUM
from PRODUCTE p 
where p.DESCRIPCIO like '%tennis%';
```


## Query 3
Muestre el código, nombre, área y teléfono de los clientes de la empresa.
```sql
use empresa;
select c.CLIENT_COD as Codigo,c.NOM as Nombre ,c.AREA as Area_De_Trabajo, c.TELEFON
from CLIENT c;

```
## Query 4
Muestre los clientes (código, nombre, ciudad) que no son del área telefónica
636.
```sql
use empresa;
select c.CLIENT_COD as Codigo,c.NOM as Nombre ,c.CIUTAT as Ciudad, 
from CLIENT c
where Area not like '636%';

```
## Query 5
Muestre las órdenes de compra de la tabla de pedidos (código, fechas de
orden y de envío).
```sql
use empresa;
select c.COM_NUM as Codigo ,c.COM_DATA as Fecha_Orden,c.DATA_TRAMESA as Fecha_Envio
from COMANDA c;

```


# VideoClub

## Query 6
 Lista de nombres y teléfonos de los clientes.
```sql
use videoclub;
select c.Nom,c.Telefon
from CLIENT c;

```
## Query 7
Lista de fechas e importes de las facturas.
```sql
use videoclub;
select f.Data, f.Import
from FACTURA f;
```
## Query 8
Lista de productos (descripción) facturados en la factura número 3.
```sql
use videoclub;
select df.Descripcio
from DETALLFACTURA df
where df.CodiFactura = 3


```
## Query 9
 Lista de facturas ordenada de forma decreciente por importe.
```sql
use videoclub;
select f.Import
from FACTURA f
order by Import desc;

```
## Query 10
Lista de los actores cuyo nombre comience por X.
```sql
use videoclub;
select a.Nom
from ACTOR a
where a.Nom like 'x%'

```
# Extras

## Query 11
Añadiendo Actores que empiezen con X su nombre
```sql
-- añado actores con letra x
use videoclub;
insert into ACTOR (Nom,CodiActor)
values	("Xordi",6);
```

## Query 12
Eliminamos un registro o mas de uno
```sql
-- borrar un registro xordi 
use videoclub;
Delete 
from ACTOR
where Nom ="xordi";
-- where CodiActor = 4 or CodiActor = 5;
```

## Query 13

```sql
-- elimino a ximo y xavi a la vez
use videoclub;
Delete 
from ACTOR
where CodiActor in (4,5); 
```

## Query 14

```sql
-- añadir varios usuarios a la vez
use videoclub;
insert into ACTOR (Nom,CodiActor)
values	("Xordi",6),("xavi",5);
```

## Query 15

```sql
-- cambiar un registro sin eliminarlo
use videoclub;
update ACTOR
set Nom = "Jordi"
where Nom = "Xordi";
```

## Query 16
```sql
use empresa;
-- en que años hemos dado de alta a trabajadores
select distinct year(DATA_ALTA)
from EMP;

```

## Query 17
```sql
-- que categorias de empleados hay actualmente
use empresa;
select distinct OFICI
from EMP


```

## Query 18
```sql
-- que empleados van a comision
use empresa;
select *
from EMP
where COMISSIO is not null;


```
## Query 19
```sql
-- que 2 empleados se llevan mas comision
use empresa;
select *
from EMP
where COMISSIO is not null
order by COMISSIO desc
limit 2 ;

```
