Alex Asensio 
------
# Reto 1_3

Mi link de mi repositorio es este https://gitlab.com/MY-PASSWORD-1234/basedatosdam2024/-/tree/main/UD_1/Reto_1.3?ref_type=heads


# VideoClub
## Query 1
Lista todas las películas del videoclub junto al nombre de su género.

```sql
use videoclub;
select p.Titol , g.Descripcio
from PELICULA p
inner join  GENERE g on p.CodiGenere = g.CodiGenere;

```


## Query 2
Lista todas las facturas de María.


```sql
use videoclub;
select c.Nom, f.*
from CLIENT c
inner join FACTURA f on c.DNI = f.DNI
where c.Nom like "María%"

-- CONCAT() sirve para concatenar
```


## Query 3
Lista las películas junto a su actor principal.
```sql
use videoclub;
select p.Titol as Titulo_pelicula , a.Nom as Nombre_Actor
from PELICULA p
join ACTOR a on a.CodiActor = p.CodiActor;

```
## Query 4
Lista películas junto a todos los actores que la interpretaron.
```sql
use videoclub;
select p.Titol as Titulo_pelicula , a.Nom as Nombre_Actor
from PELICULA p
join INTERPRETADA i on i.CodiPeli = p.CodiPeli
join ACTOR a on a.CodiActor = i.CodiActor


```
## Query 5
Lista ID y nombres de las películas junto a los ID y nombres de sus segundas
partes.
```sql
use videoclub;
select*
From PELICULA as P1
join PELICULA as P2
on P1.CodiPeli = P2.SegonaPart ;
--este ejercicio no funciona no se muy bien como se hace la verdad
```


